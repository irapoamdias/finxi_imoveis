#!/bin/bash
NAME="finxi_imoveis" #Name app
DJANGODIR=/home/webapps/finxi_imoveis/current/
SOCKFILE=/home/webapps/finxi_imoveis/run/gunicorn.sock  # we will communicte using
NUM_WORKERS=3                                     # how many worker processes
DJANGO_SETTINGS_MODULE=finxi_imoveis.settings_homolog             # which settings file should
DJANGO_WSGI_MODULE=finxi_imoveis.wsgi                     # WSGI module name
TIMEOUT=200
# Activate the virtual environment
cd $DJANGODIR
source /home/webapps/finxi_imoveis/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do
# not use --daemon)
exec /home/webapps/finxi_imoveis/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
	--name $NAME \
    --timeout $TIMEOUT \
	--workers $NUM_WORKERS \
	--bind=unix:$SOCKFILE \
	--log-level=debug \
	--log-file=-
