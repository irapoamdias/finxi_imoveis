clean:
	find . -name "*.pyc" -exec rm -rf {} \;
run: clean
	python3 manage.py runserver 0.0.0.0:8000
migrate:
	python3 manage.py migrate
migrations:
	python3 manage.py makemigrations
install:
	pip3 install -r requirements.txt
	make migrate
user:
	python3 manage.py createsuperuser

tests:
	python3 manage.py test --settings=finxi_imoveis.settings_tests

shell:
	python3 manage.py shell

initial_deploy:
	cap $(stage) setup:install_requirements_server
	cap $(stage) deploy
	cap $(stage) setup:create_folders
	cap $(stage) setup:install_requirements
	cap $(stage) setup:conf_files
	cap $(stage) setup:migrations
	cap $(stage) setup:collect_static
	cap $(stage) setup:restart_app

deploy:
	cap $(stage) deploy
	cap $(stage) setup:install_requirements
	cap $(stage) setup:migrations
	cap $(stage) setup:collect_static
	cap $(stage) setup:restart_app

fast-deploy:
	cap $(stage) deploy
	cap $(stage) setup:restart_app