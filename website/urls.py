from django.conf.urls import url
from django.contrib import admin

from .views import index, ProcurarImoveis

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^procurar/$', ProcurarImoveis.as_view(), name='procurar'),
]
