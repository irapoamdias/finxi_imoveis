from django.conf import settings
from .forms import SearchForm

def get_search_form(request):
    return{'form_search': SearchForm()}