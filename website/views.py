import json
from django.shortcuts import render
from django.views.generic import View
from django.conf import settings
from .forms import SearchForm
from imoveis.models import Imovel
from core.models import UF, Cidade
from core.utils import BuscarLatitudeLongitude

def index(request):
    
    imoveis = Imovel.objects.filter(ativo=True)

    return render(request, 'website/index.html', {'imoveis':imoveis})

class ProcurarImoveis(View):

    def get_latitude_longitude_do_bairro(self, bairro, cidade, estado):
        manager = BuscarLatitudeLongitude(bairro, cidade, estado)
        latitude, longitude = manager.get_latitude_longitude()
        return latitude, longitude

    def get_imoveis_do_bairro(self, bairro, cidade, estado):
        filtro = {}
        filtro.update({'ativo':True})
        filtro.update({'estado':estado})
        filtro.update({'cidade':cidade})
        if bairro:
            filtro.update({'bairro':bairro})
        imoveis = Imovel.objects.filter(**filtro)
        return imoveis

    def get(self, request, *args, **kwargs):
        form_search = SearchForm(request.GET)
        pk_estado = request.GET.get('estado')
        pk_cidade = request.GET.get('cidade')
        bairro = request.GET.get('bairro', None)
        estado = UF.objects.get(pk=pk_estado)
        cidade = Cidade.objects.get(pk=pk_cidade)
        imoveis_do_bairro = self.get_imoveis_do_bairro(bairro, cidade, estado)
        latitude, longitude = self.get_latitude_longitude_do_bairro(bairro, cidade, estado)
        imoveis = Imovel.objects.all()

        return render(request,'website/pesquisa.html', 
            {
                'imoveis_do_bairro':imoveis_do_bairro, 
                'imoveis':imoveis, 
                'form_search':form_search,
                'latitude':latitude, 'longitude':longitude
            })