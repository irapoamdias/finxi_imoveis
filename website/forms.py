from django import forms
from core.models import UF, Cidade

class SearchForm(forms.Form):
    estado = forms.ModelChoiceField(queryset=UF.objects.all(), 
        widget=forms.Select(attrs={'class':'form-control'}))
    cidade = forms.ModelChoiceField(queryset=Cidade.objects.all(),
        widget=forms.Select(attrs={'class':'form-control'}))
    bairro = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), 
        required=False)