from django.test import TestCase

# Create your tests here.

from django.urls import reverse


class IndexTestCase(TestCase):

    def setUp(self):
        self.url = reverse('website:index')

    def test_index_url_200(self):
        response = self.client.get(self.url)
        status_code = response.status_code
        self.assertEqual(status_code, 200)