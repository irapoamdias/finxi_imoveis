from django.conf.urls import url
from .views import dashboard
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', dashboard, name='dashboard'),
]