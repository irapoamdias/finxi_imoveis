from django.db import migrations
from django.core.management import call_command


def loadfixture(apps, schema_editor):
    call_command('loaddata', 'users.json')


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_load_data'),
    ]

    operations = [
        migrations.RunPython(loadfixture),
    ]