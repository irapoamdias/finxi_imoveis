from django.http import JsonResponse
from django.shortcuts import render
from .models import UF, Cidade

def buscar_cidades(request):
    if 'estado' in request.GET:
        estado = request.GET.get('estado')
        queryset = list(Cidade.objects.filter(federativa__pk=estado).values('id', 'name'))
    else:
        queryset = list(Cidade.objects.none())
    return JsonResponse(queryset, safe=False)