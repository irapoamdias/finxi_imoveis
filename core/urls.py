from django.conf.urls import url
from .views import buscar_cidades

urlpatterns = [
    url(r'^buscar-cidades/$', buscar_cidades, name='buscar_cidades'),
]