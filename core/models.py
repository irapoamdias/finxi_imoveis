from django.db import models


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True

class UF(models.Model):
    '''
        @UF: Data model with all states
    '''
    cod   = models.IntegerField(unique=True)
    sigla = models.CharField(max_length=2)
    name  = models.CharField(max_length=50)
    

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        db_table = u'uf'


class Cidade(models.Model): 
    '''
        @City: Data model with all cities.
    '''           
    federativa  = models.ForeignKey(UF, related_name='cidades')
    cod         = models.IntegerField(unique=True)
    name        = models.CharField(max_length=70)
    cep_inicial = models.IntegerField(unique=True)
    cep_final   = models.IntegerField(unique=True)
    ddd1        = models.IntegerField()
    ddd2        = models.IntegerField()
    
    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        db_table = u'cidade'