from django.conf import settings
import requests

class BuscarLatitudeLongitude(object):

    def __init__(self, rua_ou_bairro, cidade, estado):
        self.rua_ou_bairro = rua_ou_bairro
        self.cidade = cidade
        self.estado = estado

    def get_response(self):
        rota = settings.GOOGLE_MAPS_API_ROUTE
        query = "address=%s+%s+%s" % (self.rua_ou_bairro, self.cidade, self.estado)
        response = requests.get(rota % query)
        return response

    def get_latitude_longitude(self):
        response = self.get_response()
        dados = response.json()
        latitude = dados['results'][0]['geometry']['location']['lat']
        longitude = dados['results'][0]['geometry']['location']['lng']
        return latitude, longitude