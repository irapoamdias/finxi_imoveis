$(document).ready(function(){

    if($('#id_estado').val() == ""){
        $('#id_cidade option[value!="0"]').remove(); 
    } else{ //verificando se existe municipio selecionado
        if($('#id_cidade').val() != ""){
            var municipio = $('#id_cidade').val();
            buscar_cidade($('#id_estado').val(), municipio);
        }else{
            buscar_cidade($('#id_estado').val(),'');
        }
    }

    $("form" ).on("change","#id_estado", function() {
        buscar_cidade($(this).val(), $(this).attr('id'), '');
    });

    $("form" ).on("change","#id_cep", function() {
        buscar_cep($(this).val());
    });


    function buscar_cidade(estado, selected_citie){
        var request_url = "/core/buscar-cidades/";
        if (estado == ""){
            $('#id_cidade option[value!="0"]').remove();
            $("#id_cidade").html('<option value="">Município</option>');
        }
        else{
            $.ajax({
                type: "GET",
                url: request_url,
                data: {estado: estado},
                dataType: "json",
                success: function(json){
                    var options = "";
                    var valor = "";
                    options += '<option value="">Município</option>';
                    $.each(json, function(key, obj){
                        options += '<option value="' + obj.id + '">' + obj.name + '</option>';
                    });
                    $("#id_cidade").html(options);
                    $("#id_cidade").val(selected_citie);
                },
            });
        }
    }

    function buscar_cep(cep) {
        var cepVal = cep.replace("-", "");
        if (cepVal.length === 8) {
            requestAddressByCep(cepVal);
        }
    }

    function requestAddressByCep(cepVal) {
        var request = $.ajax({
          url: "https://api.postmon.com.br/v1/cep/" + cepVal,
          method: "GET"
        });

        request.done(function( resp ) {
            populateAddressFields(resp);
        });

        request.fail(function( resp ) {
          alert(resp.statusText);
        });
    }

    function populateAddressFields(address, id) {
        $("#id_rua").val(address.logradouro);
        $("#id_estado").val(address.estado_info.codigo_ibge);
        $("#id_bairro").val(address.bairro);
        var id_estado = $("#id_estado").attr('id');
        var val_city = address.cidade_info.codigo_ibge;
        buscar_cidade(address.estado_info.codigo_ibge, val_city);
    }
    
});
