from django.test import TestCase

from .utils import BuscarLatitudeLongitude

from decimal import Decimal

class BuscarLatitudeLongitudeTestCase(TestCase):

    def setUp(self):
        self.bairro = 'Itanhangá'
        self.rua = 'Estrada do Itanhangá'
        self.cidade = 'Rio de Janeiro'
        self.estado = 'Rio de Janeiro'

    def test_get_status_code(self):
        manager = BuscarLatitudeLongitude(self.bairro, self.cidade, self.estado)
        response = manager.get_response()
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_busca_por_bairro(self):
        manager = BuscarLatitudeLongitude(self.bairro, self.cidade, self.estado)
        latitude, longitude = manager.get_latitude_longitude()
        self.assertIs(type(latitude), float)
        self.assertIs(type(longitude), float)

    def test_busca_por_rua(self):
        manager = BuscarLatitudeLongitude(self.rua, self.cidade, self.estado)
        latitude, longitude = manager.get_latitude_longitude()
        self.assertIs(type(latitude), float)
        self.assertIs(type(longitude), float)
