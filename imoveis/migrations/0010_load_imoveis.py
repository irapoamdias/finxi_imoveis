from django.db import migrations
from django.core.management import call_command


def loadfixture(apps, schema_editor):
    call_command('loaddata', 'imoveis.json')


class Migration(migrations.Migration):

    dependencies = [
        ('imoveis', '0009_auto_20170920_1227'),
    ]

    operations = [
        migrations.RunPython(loadfixture),
    ]