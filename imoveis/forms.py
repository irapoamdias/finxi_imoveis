from django import forms
from django.forms.models import inlineformset_factory

from .models import Imovel, Foto

class ImovelForm(forms.ModelForm):
    fotos = forms.FileField(label='Fotos', 
        widget=forms.FileInput(attrs={'multiple':True}), required=False)

    class Meta:
        model = Imovel
        fields = [
            'titulo',
            'descricao',
            'cep',
            'rua',
            'numero',
            'complemento',
            'bairro',
            'cidade',
            'estado',
            'telefone1',
            'telefone2',
            'proprietario',
            'foto',
        ]
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}), 
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),

            'cep': forms.TextInput(attrs={'class': 'form-control zip_code', 
                'placeholder': 'Cep'}),
            'rua':forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'numero':forms.TextInput(attrs={'class': 'form-control'}),
            'complemento':forms.TextInput(attrs={'class': 'form-control'}),
            'bairro':forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'cidade':forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'estado':forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'telefone1':forms.TextInput(attrs={'class': 'form-control'}),
            'telefone2':forms.TextInput(attrs={'class': 'form-control'}),
            'proprietario':forms.TextInput(attrs={'class': 'form-control'}),
        }

class FotoForm(forms.ModelForm):

    class Meta:
        model = Foto
        fields = ['foto',]

FotoFormSet = inlineformset_factory(Imovel, Foto, form=FotoForm, extra=0)

