from django.test import TestCase, Client

from .models import Imovel
from core.models import UF, Cidade
from django.contrib.auth.models import User

class ImovelTestCase(TestCase):

    def setUp(self):
        user = User.objects.create_superuser(username='foo',email='foo@foo.com', password='foo')
        self.client = Client()
        self.client.login(username='foo', password='foo')
        uf = UF.objects.get(cod=33) # Rio de Janeiro
        cidade = Cidade.objects.get(cod=3300704) # Cabo Frio
        self.dict_imovel = {
            'titulo': 'foo',
            'descricao': 'foo',
            'cep':'22753005',
            'rua':'Rua Geraldo de Abreu',
            'numero': '22222',
            'complemento': 'foo',
            'bairro':'Jardim Excelsior',
            'cidade': cidade,
            'estado': uf,
            'telefone1':'1111111',
            'proprietario':'foo',
        }

    def test_create_imovel(self):
        response = self.client.post('/imoveis/novo/',self.dict_imovel)
        self.assertEqual(response.status_code, 200)

