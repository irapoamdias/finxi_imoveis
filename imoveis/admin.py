from django.contrib import admin

from .models import Imovel

class ImovelAdmin(admin.ModelAdmin):
    list_display = ['foto_thumbnail','titulo',]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        self.readonly_fields = [field.name 
            for field in filter(lambda f: not f.auto_created, self.model._meta.fields)]
        return True

    def has_delete_permission(self, request, obj=None):
        return False

    def foto_thumbnail(self, obj):
        return u'<img src="%s" />' % obj.foto.url
    

    foto_thumbnail.short_description = 'Imagem'
    foto_thumbnail.allow_tags = True


admin.site.register(Imovel, ImovelAdmin)
