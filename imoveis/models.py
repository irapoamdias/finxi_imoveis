import json
from django.db import models
from django.db.models.signals import post_save
from django.conf import settings
from django.urls import reverse
from core.models import TimeStampedModel
from core.utils import BuscarLatitudeLongitude

import requests
from easy_thumbnails.fields import ThumbnailerImageField


class Imovel(TimeStampedModel):
    titulo = models.CharField('Título', max_length=255)
    descricao = models.TextField()
    cep = models.CharField('Cep', max_length=255)
    rua = models.CharField('Rua', max_length=255)
    numero = models.CharField('Número', max_length=10)
    complemento = models.CharField('Complemento', max_length=255, blank=True, null=True)
    bairro = models.CharField('Bairro', max_length=255)
    cidade = models.ForeignKey('core.Cidade')
    estado = models.ForeignKey('core.UF')
    telefone1 = models.CharField(max_length=20)
    telefone2 = models.CharField(max_length=20, blank=True, null=True)
    proprietario = models.CharField(max_length=255)
    foto = ThumbnailerImageField(upload_to='fotos/', resize_source=dict(size=(150, 150)))
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    ativo = models.BooleanField(default=True)

    def __str__(self):
        return self.titulo

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        manager = BuscarLatitudeLongitude(self.rua, self.cidade.name, self.estado.name)
        latitude, longitude = manager.get_latitude_longitude()
        self.latitude = latitude
        self.longitude = longitude
        super(Imovel, self).save(force_insert=False, force_update=False, 
            using=None, update_fields=None)

    def get_absolute_url(self):
        return reverse('imoveis:editar_imovel', kwargs={'pk': self.pk})

    def get_detail_url(self):
        return reverse('imoveis:detalhes', kwargs={'pk': self.pk})       

class Foto(TimeStampedModel):
    foto = models.ImageField(upload_to='fotos/')
    imovel = models.ForeignKey('Imovel', related_name='fotos')

