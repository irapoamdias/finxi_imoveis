from django.conf.urls import url
from django.contrib import admin

from .views import lista_de_imoveis, novo_imovel, editar_imovel, detalhes

urlpatterns = [
    url(r'^$', lista_de_imoveis, name='lista_de_imoveis'),
    url(r'^novo/$', novo_imovel, name='novo_imovel'),
    url(r'^(?P<pk>[0-9]+)/editar/$', editar_imovel, name='editar_imovel'),
    url(r'^(?P<pk>[0-9]+)/info/$', detalhes, name='detalhes'),
]
