from django.core.urlresolvers import reverse as r
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from .forms import ImovelForm, FotoFormSet
from .models import Imovel, Foto

@login_required
def lista_de_imoveis(request):
    imoveis = Imovel.objects.all()
    return render(request, 'imoveis/lista.html', {'imoveis':imoveis})

@login_required
def novo_imovel(request):

    if request.method == 'POST':
        form = ImovelForm(request.POST, request.FILES)
        if form.is_valid():
            imovel = form.save()
            for f in request.FILES.getlist('fotos'):
                Foto.objects.create(imovel=imovel, foto=f)

            return HttpResponseRedirect(r('imoveis:lista_de_imoveis'))
    else:
        form = ImovelForm()

    return render(request,'imoveis/novo.html', {'form':form})


@login_required
def editar_imovel(request, pk):
    imovel = get_object_or_404(Imovel, pk=pk)
    if request.method == 'POST':
        form = ImovelForm(request.POST, request.FILES, instance=imovel)
        formset = FotoFormSet(request.POST, request.FILES, instance=imovel)
        if form.is_valid() and formset.is_valid():
            imovel = form.save()
            formset.save()
            for f in request.FILES.getlist('fotos'):
                Foto.objects.create(imovel=imovel, foto=f)
            return HttpResponseRedirect(r('imoveis:lista_de_imoveis'))
    else:
        form = ImovelForm(instance=imovel)
        formset = FotoFormSet(instance=imovel)
    return render(request, 'imoveis/editar.html', 
        {'form':form, 'formset':formset})

def detalhes(request, pk):
    imovel = get_object_or_404(Imovel, pk=pk)
    return render(request, 'imoveis/detalhes.html', {'imovel':imovel})

