from .settings import *

import sys

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':':memory:',
    }
}


# TESTS_IN_PROGRESS = False
# if 'test' in sys.argv[1:]:
#     DEBUG = False
#     TEMPLATE_DEBUG = False
#     TESTS_IN_PROGRESS = True
#     MIGRATION_MODULES = DisableMigrations()