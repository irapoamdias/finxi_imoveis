from .settings import *

DEBUG = False
ALLOWED_HOSTS = ['54.227.48.17']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': 'localhost', #or host database
        'PORT': '5432',
        'NAME': 'dbfinxi',
        'USER': 'finxi',
        'PASSWORD': '123123',
    }
}


MEDIA_ROOT = '/home/webapps/finxi_imoveis/media'
STATIC_ROOT = '/home/webapps/finxi_imoveis/static_collected'